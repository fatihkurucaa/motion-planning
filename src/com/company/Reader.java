package com.company;

import com.company.Point.Point;
import com.company.Point.PointMap;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class Reader {
    private PointMap pointMap;
    private ArrayList<ArrayList<String>> bitmap;

    Reader() {
        pointMap = new PointMap();
        bitmap = new ArrayList<>();
    }

    public void reader(String filepath) throws IOException {
        BufferedReader csvReader = new BufferedReader(new FileReader(filepath));
        String row;

        csvReader.readLine();
        csvReader.readLine();

        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",");
            ArrayList<String> datas = new ArrayList<>(Arrays.asList(data));
            System.out.println();
            bitmap.add(datas);
        }
        csvReader.close();

        for (int i = 0; i < bitmap.size(); i++) {
            pointMap.getPointMap().add(new ArrayList<>());
            for (int j = 0; j < bitmap.get(i).size(); j++) {
                Point temp = new Point(j, i, bitmap.get(i).get(j).equals("1"));
                pointMap.getPointMap().get(i).add(temp);
            }
        }
    }

    public PointMap getPointMap() {
        return pointMap;
    }

}
