package com.company;

import org.graphstream.algorithm.AStar;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.MultiGraph;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {

    public static void main(String[] args) throws IOException {
        System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
        String filePath = "C:\\Users\\Metalle\\IdeaProjects\\MotionPlanning\\src\\resources\\map01.txt";
        //String filePath = "C:\\Users\\Metalle\\IdeaProjects\\MotionPlanning\\src\\resources\\map02.txt";
        //String filePath = "C:\\Users\\Metalle\\IdeaProjects\\MotionPlanning\\src\\resources\\map03.txt";
        //String filePath = "C:\\Users\\Metalle\\IdeaProjects\\MotionPlanning\\src\\resources\\map04.txt";
        //String filePath = "C:\\Users\\Metalle\\IdeaProjects\\MotionPlanning\\src\\resources\\custom01.txt";
        //String filePath = "C:\\Users\\Metalle\\IdeaProjects\\MotionPlanning\\src\\resources\\custom02.txt";

        long startTime = System.nanoTime();

        Bitmap bitmap = new Bitmap(filePath);
        bitmap.jobs();
        bitmap.findRelations();

        long betweenTime = System.nanoTime();

        Graph graph = new MultiGraph("complex");
        graph.addAttribute("ui.quality");
        graph.addAttribute("ui.antialias");

        for(int i=0; i<bitmap.getNodes().size(); i++) {
            graph.addNode(Integer.toString(i));
        }

        for(int i=0; i<bitmap.getNodes().size(); i++) {
            int finalI = i;
            bitmap.getNodes().get(i).getConnectedNodes().forEach(connection -> {
                if(!graph.getNode(connection).hasEdgeBetween(finalI) && !graph.getNode(finalI).hasEdgeBetween(connection))
                    graph.addEdge(connection+finalI+"asd"+finalI+connection, finalI, connection);
            });
        }
        long endTime = System.nanoTime();
        long timeAll = endTime - startTime;
        long timeTillGUI = betweenTime - startTime;
        System.out.println("Time passed = " + timeAll/1000000000 + " secs");
        System.out.println("Bitmap dimensions: " + bitmap.getHeight() + "x" + bitmap.getLength());
        System.out.println("Nodes created = " + graph.getNodeCount());
        int sum = 0;
        for(int i=0; i<bitmap.getNodes().size(); i++) {
            sum += bitmap.getNodes().get(i).getPointMap().getPointMap().size();
        }
        int lowest = bitmap.getNodes().get(0).getPointMap().getPointMap().size();
        for(int i=0; i<bitmap.getNodes().size(); i++) {
            if(bitmap.getNodes().get(i).getPointMap().getPointMap().size() < lowest) {
                lowest = bitmap.getNodes().get(i).getPointMap().getPointMap().size();
            }
        }
        System.out.println("The biggest node's dimensions: " + bitmap.getNodes().get(0).getPointMap().getPointMap().size() + "x" + bitmap.getNodes().get(0).getPointMap().getPointMap().get(0).size());
        System.out.println("The smallest node's dimensions: " + lowest + "x" + lowest);
        System.out.println("Average node size: " + (double)sum / (double)graph.getNodeCount());
        graph.display();

        BufferedReader csvReader = new BufferedReader(new FileReader(filePath));
        String start = csvReader.readLine();
        String[] data = start.split(",");
        int startX = Integer.parseInt(data[0]); int startY = Integer.parseInt(data[1]);

        start = csvReader.readLine();
        data = start.split(",");
        int finishX = Integer.parseInt(data[0]); int finishY = Integer.parseInt(data[1]);
        int startNode = -1, finishNode = -1;

        for(int i=0; i<bitmap.getNodes().size(); i++) {
            if(bitmap.getNodes().get(i).hasCoordinates(startX, startY)){
                startNode = i; //bitmap.getNodes().get(i);
            } else if(bitmap.getNodes().get(i).hasCoordinates(finishX, finishY)){
                finishNode = i; //bitmap.getNodes().get(i);
            }
        }

        if(startNode == -1 || finishNode == -1) {
            System.out.println("No path available for the given bitmap and points!");
        } else {
            AStar aStar = new AStar(graph);
            aStar.compute(String.valueOf(startNode), String.valueOf(finishNode));
            String path = aStar.getShortestPath().toString();
            System.out.println(path.substring(0, path.length() / 4));
            System.out.println(path.substring(path.length() / 4, path.length() / 2));
            System.out.println(path.substring(path.length() / 2, 3 * path.length() / 4));
            System.out.println(path.substring(3 * path.length() / 4));
            System.out.println("How many nodes from start to end: " + aStar.getShortestPath().getNodeCount());
            System.out.println("Start node: " + String.valueOf(startNode) + "  End node: " + String.valueOf(finishNode));
            AtomicInteger totalBits = new AtomicInteger();
            aStar.getShortestPath().getEachNode().forEach(node -> {
                totalBits.addAndGet(bitmap.getNodes().get(Integer.parseInt(node.getId())).getPointMap().getPointMap().size());
            });
            System.out.println("Total distance as bits: " + totalBits);
            aStar.getShortestPath().getEachEdge().forEach(edge -> {
                edge.setAttribute("ui.style", "fill-color: red;");
            });
        }

        ///////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////


    }


}
