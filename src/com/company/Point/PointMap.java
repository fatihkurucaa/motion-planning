package com.company.Point;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class PointMap {
    private ArrayList<ArrayList<Point>> pointMap;

    public PointMap(int y, int x) {
        pointMap = new ArrayList<>();
        for(int i=0; i<y; i++) {
            pointMap.add(new ArrayList<>());
            for(int j=0; j<x; j++) {
                pointMap.get(i).add(new Point(0,0,false));
            }
        }
    }

    public PointMap(int y) {
        pointMap = new ArrayList<>();
        for(int i=0; i<y; i++) {
            pointMap.add(new ArrayList<>());
        }
    }

    public PointMap() {
        pointMap = new ArrayList<>();
    }

    public PointMap(ArrayList<ArrayList<Point>> pointMap) {
        this.pointMap = pointMap;
    }

    public void print() {
        pointMap.forEach(pointRow -> {
            pointRow.forEach(point -> {
                System.out.print("[X:" + point.getX() + " Y:" + point.getY() + " Value:" + point.isValue() + "] ");
            });
            System.out.println();
        });
    }

    public void printToFile() throws IOException {
        File file = new File("C:\\Users\\Metalle\\IdeaProjects\\MotionPlanning\\src\\resources\\output.txt");
        final FileWriter fr = new FileWriter(file);

        pointMap.forEach(pointRow -> {
            pointRow.forEach(point -> {
                try {
                    fr.append("[X:" + point.getX() + " Y:" + point.getY() + " Value:" + point.isValue() + "] ");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            try {
                fr.append('\n');
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public boolean containsCoordinate(int x, int y) {
        int[] ret = {0};

        pointMap.forEach(points -> points.forEach(point -> {
            if(point.getX() == x && point.getY() == y)
                ret[0] = 1;
        }));
        return ret[0] == 1;
    }

    public boolean hasSpace() {
        final boolean[] hasSpa = {false};

        pointMap.forEach(pointRow -> {
            pointRow.forEach(point -> {
                if(!point.isValue())
                    hasSpa[0] = true;
            });
        });
        return hasSpa[0];
    }

    public boolean hasObstacles() {
        final boolean[] hasObs = {false};

        pointMap.forEach(pointRow -> {
            pointRow.forEach(point -> {
                if(point.isValue())
                    hasObs[0] = true;
            });
        });
        return hasObs[0];
    }

    public boolean noSpace() {
        final boolean[] noSpa = {true};

        pointMap.forEach(pointRow -> {
            pointRow.forEach(point -> {
                if(point.isValue())
                    noSpa[0] = false;
            });
        });
        return noSpa[0];
    }

    public boolean noObstacles() {
        final boolean[] noObs = {true};

        pointMap.forEach(pointRow -> {
            pointRow.forEach(point -> {
                if(!point.isValue())
                    noObs[0] = false;
            });
        });
        return noObs[0];
    }

    public ArrayList<ArrayList<Point>> getPointMap() {
        return pointMap;
    }
}
