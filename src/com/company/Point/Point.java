package com.company.Point;

import java.util.Objects;

public class Point {
    private int x;
    private int y;
    private boolean value;

    public Point(int x, int y, boolean value) {
        this.x = x;
        this.y = y;
        this.value = value;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return x == point.x &&
                y == point.y &&
                value == point.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, value);
    }
}
