package com.company;

import com.company.Point.PointMap;

import java.io.IOException;
import java.util.ArrayList;

public class Bitmap {
    private ArrayList<PointMap> bitmaps;
    private ArrayList<PointMap> passableMaps;
    private ArrayList<Node> nodes;
    private int length;
    private int height;

    public Bitmap(String filepath) throws IOException {
        Reader bitmapReader = new Reader();
        bitmaps = new ArrayList<>();
        passableMaps = new ArrayList<>();
        nodes = new ArrayList<>();
        bitmapReader.reader(filepath);
        bitmaps.add(bitmapReader.getPointMap());
        height = bitmaps.get(0).getPointMap().size();
        length = bitmaps.get(0).getPointMap().get(0).size();
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void jobs() {
        if (!isFinished()) {
            for (int i = 0; i < bitmaps.size(); i++) {
                if (bitmaps.get(i).hasObstacles() && bitmaps.get(i).hasSpace()) {
                    int y = bitmaps.get(i).getPointMap().size(), x = bitmaps.get(i).getPointMap().get(0).size();
                    if (isEven(x) && isEven(y)) {
                        divisionEven(i);
                        break;
                    } else if (!isEven(x) && !isEven(y)) {
                        divisionOdd(i);
                        break;
                    } else if (!isEven(x) && isEven(y)) {
                        divisionEven(i);
                        break;
                    } else if (isEven(x) && !isEven(y)) {
                        divisionOdd(i);
                        break;
                    } else if (x == 1) {
                        divisionLengthOne(i);
                        break;
                    } else if (y == 1) {
                        divisionHeightOne(i);
                        break;
                    }
                }
            }
            jobs();
        } else {

            for (int i = 0; i < bitmaps.size(); i++) {
                if(bitmaps.get(i).getPointMap().size() > 0 && bitmaps.get(i).getPointMap().get(0).size() > 0)
                    if (!bitmaps.get(i).getPointMap().get(0).get(0).isValue())
                        passableMaps.add(bitmaps.get(i));
            }
        }
    }

    public ArrayList<PointMap> getBitmaps() {
        return bitmaps;
    }

    public boolean isFinished() {
        final boolean[] isFin = {true};

        bitmaps.forEach(bitmap -> {
            if (bitmap.hasObstacles() && bitmap.hasSpace())
                isFin[0] = false;
        });
        return isFin[0];
    }

    public boolean isEven(int a) {
        if (a % 2 != 0) {
            return false;
        }
        return true;
    }

    public void divisionEven(int iterator) {
        ArrayList<PointMap> temps = new ArrayList<>();
        int height = bitmaps.get(iterator).getPointMap().size() / 2;
        int tempIterator = 0;

        for (int i = 0; i < 4; i++) {
            temps.add(new PointMap(height));
        }

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < bitmaps.get(iterator).getPointMap().get(i).size() / 2; j++) {
                temps.get(0).getPointMap().get(i).add(bitmaps.get(iterator).getPointMap().get(i).get(j));
            }
            for (int j = bitmaps.get(iterator).getPointMap().get(i).size() / 2; j < bitmaps.get(iterator).getPointMap().get(i).size(); j++) {
                temps.get(1).getPointMap().get(i).add(bitmaps.get(iterator).getPointMap().get(i).get(j));
            }
        }
        for (int i = height; i < bitmaps.get(iterator).getPointMap().size(); i++, tempIterator++) {
            for (int j = 0; j < bitmaps.get(iterator).getPointMap().get(i).size() / 2; j++) {
                temps.get(2).getPointMap().get(tempIterator).add(bitmaps.get(iterator).getPointMap().get(i).get(j));
            }
            for (int j = bitmaps.get(iterator).getPointMap().get(i).size() / 2; j < bitmaps.get(iterator).getPointMap().get(i).size(); j++) {
                temps.get(3).getPointMap().get(tempIterator).add(bitmaps.get(iterator).getPointMap().get(i).get(j));
            }
        }

        bitmaps.addAll(temps);
        bitmaps.remove(iterator);
    }

    // look at y
    public void divisionOdd(int iterator) {
        ArrayList<PointMap> temps = new ArrayList<>();
        int[] heights = new int[2];
        int tempIterator = 0;

        heights[0] = bitmaps.get(iterator).getPointMap().size() / 2;
        heights[1] = bitmaps.get(iterator).getPointMap().size() / 2 + 1;

        for (int i = 0; i < 2; i++) {
            temps.add(new PointMap(heights[0]));
        }
        for (int i = 0; i < 2; i++) {
            temps.add(new PointMap(heights[1]));
        }

        for (int i = 0; i < heights[0]; i++) {
            for (int j = 0; j < bitmaps.get(iterator).getPointMap().get(i).size() / 2; j++) {
                temps.get(0).getPointMap().get(i).add(bitmaps.get(iterator).getPointMap().get(i).get(j));
            }
            for (int j = bitmaps.get(iterator).getPointMap().get(i).size() / 2; j < bitmaps.get(iterator).getPointMap().get(i).size(); j++) {
                temps.get(1).getPointMap().get(i).add(bitmaps.get(iterator).getPointMap().get(i).get(j));
            }
        }
        for (int i = heights[0]; i < bitmaps.get(iterator).getPointMap().size(); i++, tempIterator++) {
            for (int j = 0; j < bitmaps.get(iterator).getPointMap().get(i).size() / 2; j++) {
                temps.get(2).getPointMap().get(tempIterator).add(bitmaps.get(iterator).getPointMap().get(i).get(j));
            }
            for (int j = bitmaps.get(iterator).getPointMap().get(i).size() / 2; j < bitmaps.get(iterator).getPointMap().get(i).size(); j++) {
                temps.get(3).getPointMap().get(tempIterator).add(bitmaps.get(iterator).getPointMap().get(i).get(j));
            }
        }

        bitmaps.addAll(temps);
        bitmaps.remove(iterator);
    }

    public void divisionHeightOne(int iterator) {
        ArrayList<PointMap> temps = new ArrayList<>();

        for (int i = 0; i < 2; i++) {
            temps.add(new PointMap(1));
        }

        for (int j = 0; j < bitmaps.get(iterator).getPointMap().get(0).size() / 2; j++) {
            temps.get(0).getPointMap().get(0).add(bitmaps.get(iterator).getPointMap().get(0).get(j));
        }
        for (int j = bitmaps.get(iterator).getPointMap().get(0).size() / 2; j < bitmaps.get(iterator).getPointMap().get(0).size(); j++) {
            temps.get(1).getPointMap().get(0).add(bitmaps.get(iterator).getPointMap().get(0).get(j));
        }

        bitmaps.addAll(temps);
        bitmaps.remove(iterator);
    }

    public void divisionLengthOne(int iterator) {
        ArrayList<PointMap> temps = new ArrayList<>();
        int height = bitmaps.get(iterator).getPointMap().size() / 2;

        for (int i = 0; i < 2; i++) {
            temps.add(new PointMap(height));
        }

        for (int i = 0; i < height; i++) {
            temps.get(0).getPointMap().get(i).add(bitmaps.get(iterator).getPointMap().get(i).get(0));
        }
        for (int i = height; i < bitmaps.get(iterator).getPointMap().size(); i++) {
            temps.get(1).getPointMap().get(i).add(bitmaps.get(iterator).getPointMap().get(i).get(0));
        }

        bitmaps.addAll(temps);
        bitmaps.remove(iterator);
    }

    public void findRelations() {
        passableMaps.forEach(bitmap -> {
            nodes.add(new Node(bitmap));
        });
        for (Node node : nodes) {
            if(node.getLeftside().get(0).getX() != 0)
                node.getLeftside().forEach(point -> {
                    for(int i=0; i<nodes.size(); i++) {
                        if(nodes.get(i).getPointMap().containsCoordinate(point.getX()-1, point.getY()))
                            node.getConnectedNodes().add(i);
                    }
                });
            if(node.getUpside().get(0).getY() != 0)
                node.getUpside().forEach(point -> {
                    for(int i=0; i<nodes.size(); i++) {
                        if(nodes.get(i).getPointMap().containsCoordinate(point.getX(), point.getY()-1))
                            node.getConnectedNodes().add(i);
                    }
                });
            if(node.getLeftside().get(0).getX() != 0 && node.getUpside().get(0).getY() != 0)
                node.getLeftside().forEach(point -> {
                    for(int i=0; i<nodes.size(); i++) {
                        if(nodes.get(i).getPointMap().containsCoordinate(point.getX()-1, point.getY()-1))
                            node.getConnectedNodes().add(i);
                    }
                });
            if(node.getLeftside().get(0).getX() != 0 && node.getDownside().get(0).getY() != height-1)
                node.getLeftside().forEach(point -> {
                    for(int i=0; i<nodes.size(); i++) {
                        if(nodes.get(i).getPointMap().containsCoordinate(point.getX()-1, point.getY()+1))
                            node.getConnectedNodes().add(i);
                    }
                });
        }
        nodes.forEach(Node::removeDuplicate);
    }

    public ArrayList<Node> getNodes() {
        return nodes;
    }
}
