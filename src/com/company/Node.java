package com.company;

import com.company.Point.Point;
import com.company.Point.PointMap;

import java.util.ArrayList;

public class Node {
    private PointMap pointMap;
    private ArrayList<Integer> connectedNodes;

    public Boolean hasCoordinates(int x, int y) {
        Boolean[] returnVal = {false};
        pointMap.getPointMap().forEach(points -> {
            points.forEach(point -> {
                if(point.getX() == x && point.getY() == y)
                    returnVal[0] = true;
            });
        });
        return returnVal[0];
    }

    public ArrayList<Integer> getConnectedNodes() {
        return connectedNodes;
    }

    public void setConnectedNodes(ArrayList<Integer> connectedNodes) {
        this.connectedNodes = connectedNodes;
    }

    public Node (PointMap pointMap){
        this.pointMap = pointMap;
        connectedNodes = new ArrayList<>();
    }

    public ArrayList<Point> getUpside() {
        return new ArrayList<>(pointMap.getPointMap().get(0));
    }

    public ArrayList<Point> getDownside() {
        return new ArrayList<>(pointMap.getPointMap().get(pointMap.getPointMap().size()-1));
    }

    public ArrayList<Point> getLeftside() {
        ArrayList<Point> points = new ArrayList<>();

        pointMap.getPointMap().forEach(points1 -> {
            points.add(points1.get(0));
        });

        return points;
    }

    public ArrayList<Point> getRightside() {
        ArrayList<Point> points = new ArrayList<>();

        pointMap.getPointMap().forEach(points1 -> {
            points.add(points1.get(points1.size()-1));
        });

        return points;
    }

    public PointMap getPointMap() {
        return pointMap;
    }

    public void removeDuplicate() {
        ArrayList<Integer> temp = new ArrayList<>();

        for(Integer i : connectedNodes) {
            if(!temp.contains(i))
                temp.add(i);
        }
        connectedNodes = temp;
    }
}
